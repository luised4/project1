<%@ page import="com.models.Employee" %>
<%@ page import="java.util.List" %><%--
  Created by IntelliJ IDEA.
  User: Links
  Date: 7/6/2019
  Time: 1:46 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<title>Employees</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta http-equiv="X-UA-Compatible" content="ie=edge" />
<head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://kit.fontawesome.com/b0de26430c.js"></script>
</head>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link href="./manager.css" rel="stylesheet" />

<body>
<div class="sidenav">
    <a href="#">Pending Requests</a>
    <a href ='#'>Resolved Requests</a>
    <a href='#'>Employees</a>
    <a href='#'>Log Out</a>
</div>

<div id="container">
    <h4>Employees:</h4>
    <div class="employee">
        <div class="row">
            <div class="col">
                Employee ID:
                <div id="employeeId">

                </div>
            </div>
            <div class="col">
                Employee Name:
                <div id="employeeName">

                </div>
            </div>
            <div class="col">
                <div class="btn1">
                    <div id = "someDiv"></div>
                    <input type="submit" value="Display Reimbursement Requests" class="btn">
                </div>
            </div>


        </div>
    </div>
</div>
<script>
    class Employee {
        constructor() {
            this._id = 0;
            this._firstName = "";
            this._lastName = "";
            this._email = "";
            this._password = "";
        }

        get id() {
            return this._id;
        }

        set id(id) {
            this._id = id;
        }

        get firstName() {
            return this._firstName;
        }

        set firstName(firstName) {
            this._firstName = firstName;
        }

        get lastName() {
            return this._lastName;
        }

        set lastName(lastName) {
            this._lastName = lastName;
        }

        get email() {
            return this._email;
        }

        set email(email) {
            this._email = email;
        }

        get password() {
            return this._password;
        }

        set password(password) {
            this._password = password;
        }

        toJSON() {
            return {
                id: this._id,
                firstName: this._firstName,
                lastName: this._lastName,
                email: this._email,
                password: this._password
            };
        }
    }

    this._employeeList = [];

    function newEmployeeDisplay(){
        let employeeDisplay = document.querySelector(".employee")
        let employeeDisplayClone = employeeDisplay.cloneNode(true);
        document.getElementById("container").appendChild(employeeDisplayClone);
        employeeDisplayClone.querySelector("#employeeId").innerText = x._id;
    }

    const delta = document.querySelector(".employee");
    document.querySelector(".employee").visibility = "hidden";

    fetch('http://localhost:8080/project1attempt/servlet', {})
        .then(resp => resp.json()

        .then(json => {
            json.forEach(e => {
                const x = Object.assign(new Employee(), e);
                this._employeeList.push(x);
                console.log(x);
                JSON.stringify(x);
                let employeeDisplay = document.querySelector(".employee")
                let employeeDisplayClone = employeeDisplay.cloneNode(true);
                document.getElementById("container").appendChild(employeeDisplayClone);
                employeeDisplayClone.querySelector("#employeeId").innerText = x._id;
                employeeDisplayClone.querySelector("#employeeName").innerText = x._firstName + " " +  x._lastName;
                employeeDisplayClone.style.visibility = "visible";


            });
            render();
            delta.remove();
        })
        .catch(err => console.log('I should have been a lawyer', err))

    function render (){
        let z = document.createElement('p');
        z.innerText = this._employeeList;
        return z;
        console.log(z);
    }

    $(document).on("click", ".btn1", function () {
        $.get("servlet", function (response) {
            $("#someDiv").text(response);
            JSON.parse(response);
            console.log(response);
        })
    })


    // var ajax  = new XMLHttpRequest();
    // ajax.open("GET", "http://localhost:8080/project1attempt/employees.jsp", true);
    // ajax.send;
    //
    //         var data = ajax.responseText;
    //         // the return
    //         console.log(data);

    // var ajax = new XMLHttpRequest();
    //
    // ajax.open("GET", "http://localhost:8080/project1attempt/servlet", true);
    //
    // // send request
    // ajax.send();
    //
    // // event to get response
    // ajax.onreadystatechange = function() {
    //     // Case state is 4 e o http.status for 200, your request is OK.
    //     if (ajax.readyState == 4 && ajax.status == 200) {
    //         var data = ajax.responseText;
    //         // the return
    //         console.log(JSON.parse(data));
    //     }
    // }



    function populateList(employee){

        // List<Employee> employeeList = (List<Employee>) request.getAttribute("employees");
        // console.log(employeeList);
    }

    populateList();
</script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
