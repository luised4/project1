package com.Service;


import com.models.Reimbursement;
import com.repository.PostgresConnectionManager;
import com.repository.ReimbursementDAOImpl;

import java.util.List;

public class LuisReimbursementService {

    private PostgresConnectionManager manager;
    private ReimbursementDAOImpl dao;

    public LuisReimbursementService(){
        manager = new PostgresConnectionManager();
        manager.init();
        dao = new ReimbursementDAOImpl(manager);
    }

    public Reimbursement findOne(int id) {
        return null;
    }


    public List<Reimbursement> findAll() {
        return null;
    }


    public void update(Reimbursement obj) {

    }


    public void delete(Reimbursement obj) {

    }

    public int addReimbursement(Reimbursement obj) {
        return dao.addReimbursement(obj);

    }

    public int save(Reimbursement obj) {
        return 0;
    }
}