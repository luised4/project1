package com.Service;


import com.models.User;
import com.repository.EmployeeDAOImpl;
import com.repository.PostgresConnectionManager;

public class UserService {

	EmployeeDAOImpl userDAO;
	PostgresConnectionManager manager;

	public UserService(){
		manager = new PostgresConnectionManager();
		manager.init();
		userDAO = new EmployeeDAOImpl(manager);
	}



	public User loginUser(String username, String password) {
		System.out.println("Log in well");
		User u = new User();

		u = userDAO.getUserByCredentials(username, password);

		return u;
	}
}
