package com.Service;


import com.system.AuthenticationStatus;

public interface UserAuthentication {
    void authenticate(String username, String password,
                      AuthenticationStatus onStatusChange);
}
