package com.Service;
import com.models.Employee;

import java.util.List;

public interface UserDetailService {

    Employee getByEmail(String email);
    Employee findById (int Id);

    List<Employee> findAll();
}
