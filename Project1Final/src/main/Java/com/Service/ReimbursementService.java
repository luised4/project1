package com.Service;


import com.repository.PostgresConnectionManager;
import com.repository.ReimbursementDAOImpl;
import com.repository.Repository;
import com.models.Employee;
import com.models.Reimbursement;

import java.util.List;

public class ReimbursementService implements DataService <Reimbursement> {

    private PostgresConnectionManager manager;
    private ReimbursementDAOImpl dao;

    public ReimbursementService(){
        manager = new PostgresConnectionManager();
        manager.init();
        dao = new ReimbursementDAOImpl(manager);
    }


    private Repository<Reimbursement> reimbursementRepository;

    public void setReimbursementRepository(Repository<Reimbursement> reimbursementRepository)
    {this.reimbursementRepository = reimbursementRepository; }

    @Override
    public Reimbursement findOne(int id) {
        return null;
    }

    @Override
    public List<Reimbursement> findAllResolved() {
        return reimbursementRepository.findAllResolved();
    }

    @Override
    public List<Reimbursement> findAllReimbursements(int id) {
        return reimbursementRepository.findAllReimbursements(id);
    }

    @Override
    public List<Reimbursement> findAll() {
        return null;
    }

    @Override
    public void update(Reimbursement obj) {

    }

    @Override
    public void delete(Reimbursement obj) {

    }

    @Override
    public void create(Employee obj) {

    }

    @Override
    public int save(Reimbursement obj) {
        return 0;
    }

    @Override
    public List<Reimbursement> findAllPending()
     { return reimbursementRepository.findAllPending();}


    public int addReimbursement(Reimbursement obj) {
        return dao.addReimbursement(obj);

    }


}