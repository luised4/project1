package com.Service;

import com.models.Employee;
import com.system.AuthenticationStatus;

public class UserAuthenticationImpl implements UserAuthentication {
    private UserDetailService userService;

    public UserAuthenticationImpl(UserDetailService userService) {
        this.userService = userService;
    }

    public UserAuthenticationImpl() {

    }


    public void authenticate(String username, String password, AuthenticationStatus onStatusChange) {
        Employee u = userService.getByEmail(username);

        if(u == null) {
            onStatusChange.authStatus(u, false);
            return;
        }

        if(!u.getEmail().equals(username) || !u.getPassword().equals(password)) {
            onStatusChange.authStatus(null, false);
        } else {
            onStatusChange.authStatus(u, true);
        }
    }
}
