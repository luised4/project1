package com.Service;


import com.models.Employee;
import com.models.Reimbursement;

import java.util.List;

public interface DataService<T> {
    T findOne(int id);


    List<T>findAllResolved();
    List<T>findAllReimbursements(int id);
    List<T> findAll();
    void update(T obj);
    void delete(T obj);
    void create(Employee obj);
    int save(T obj);

    List<Reimbursement> findAllPending();
}
