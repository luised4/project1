package com.Service;

import com.models.Employee;
import com.repository.EmployeeRepository;

import java.util.List;

public class UserDetailImpl implements UserDetailService {

    private final EmployeeRepository employeeRepository;
    public UserDetailImpl (EmployeeRepository employeeRepository) {this.employeeRepository = employeeRepository;}

    @Override
    public Employee getByEmail(String email) {
        return null;
    }

    @Override
    public Employee findById(int Id) {
        return employeeRepository.findById(Id);
    }

    @Override
    public List<Employee> findAll() {
        return employeeRepository.findAll();
    }

}
