package com.web;

import com.Service.ReimbursementService;
import com.models.Reimbursement;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class PendingServlet extends HttpServlet {
    ReimbursementService rs;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json");
        List<Reimbursement> reimbursements = rs.findAllPending();
        String json = new ObjectMapper().writeValueAsString(reimbursements);
        resp.getWriter().write(json);
    }

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.service(req, resp);
    }

    @Override
    public void destroy() {
        super.destroy();
    }

    @Override
    public void init(ServletConfig config) throws ServletException {
        ServletContext context = config.getServletContext();
        String serviceName = config.getInitParameter("employee.service.pending");
        rs = (ReimbursementService) context.getAttribute(serviceName);;
    }
}
