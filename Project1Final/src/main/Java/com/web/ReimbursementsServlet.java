package com.web;

import com.Service.ReimbursementService;
import com.models.Reimbursement;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.*;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class ReimbursementsServlet extends HttpServlet {
    ReimbursementService rs;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        int id = Integer.parseInt(req.getParameter("employee_id"));
        resp.setContentType("application/json");
        List<Reimbursement> reimbursements = rs.findAllReimbursements(id);
        String json = new ObjectMapper().writeValueAsString(reimbursements);
        resp.getWriter().write(json);
    }

    @Override
    public void service(ServletRequest req, ServletResponse res) throws ServletException, IOException {
        super.service(req, res);
    }

    @Override
    public void destroy() {
        super.destroy();
    }

    @Override
    public void init(ServletConfig config) throws ServletException {
        ServletContext context = config.getServletContext();
        String serviceName = config.getInitParameter("employee.service.reimbursements");
        rs = (ReimbursementService) context.getAttribute(serviceName);
    }
}
