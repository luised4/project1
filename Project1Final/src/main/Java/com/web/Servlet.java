package com.web;

import com.Service.UserDetailService;
import com.models.Employee;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;


public class Servlet extends HttpServlet {
    UserDetailService uds;

    @Override
    public void init(ServletConfig config) throws ServletException {
        ServletContext context = config.getServletContext();
        String serviceName = config.getInitParameter("user.service.service-name");
        uds = (UserDetailService)context.getAttribute(serviceName);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    resp.setContentType("application/json");
    List<Employee> employees = uds.findAll();
    String json = new ObjectMapper().writeValueAsString(employees);
    resp.getWriter().write(json);


//    ObjectMapper mapper = new ObjectMapper();
//    List<Employee> jsonEmployees = mapper.readValue( String.valueOf(employees), new TypeReference<List<Employee>>(){});
//        Employee e = null;
//        ObjectMapper mapper = new ObjectMapper();
//        List<Employee> employees = new LinkedList<Employee>();
//    e = new ObjectMapper().readValue(req.getInputStream(), Employee.class);
//    req.setAttribute("employees", employees);
//    employees.add(e);
//        mapper.writeValue(resp.getOutputStream(), employees);



    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPut(req, resp);
    }

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.service(req, resp);
    }

    @Override
    public void destroy() {
        super.destroy();
    }


}
