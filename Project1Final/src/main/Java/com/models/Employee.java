package com.models;



import java.util.Objects;

public class Employee {
    private int Id;
    private String firstName;
    private String lastName;
    private String email;
    private String password;

    public Employee() {

    }

    public Employee(int Id, String firstName, String lastName, String email, String password) {
        this.Id = Id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.password = password;
    }


    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getId() {
        return Id;
    }

    public void setId(int Id) {
        this.Id = Id;
    }


    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }




    @Override
    public String toString() {
        return "Employee{" +
                ", employee_id='" + Id + '\'' +
                ", email='" + email + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +

                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Employee employee = (Employee) o;
        return Id == employee.Id &&

                 Objects.equals(Id, employee.Id) &&
                Objects.equals(email, employee.email) &&

                Objects.equals(firstName, employee.firstName) &&
                Objects.equals(lastName, employee.lastName) &&

                Objects.equals(password, employee.password);
    }

    @Override
    public int hashCode() {
        return Objects.hash(Id,email, firstName, lastName,Id);
    }
}


