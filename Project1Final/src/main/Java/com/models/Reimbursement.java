package com.models;

import java.sql.Date;

public class Reimbursement {
    private int id;
    private Date date;
    private String description;
    private double amount;
    private int typingId;
    private int employeeId;
    private Typing typing;
    private String lastName;

    private String firstName;


    public Reimbursement() {
    }

    public Reimbursement(int id, Date date, String description, double amount, int typingId, int employeeId, String firstName, String lastName, Typing typing) {
        this.date = date;
        this.id = id;
        this.description = description;
        this.amount = amount;
        this.typingId = typingId;
        this.employeeId = employeeId;
        this.firstName = firstName;
        this.typing = typing;
        this.lastName = lastName;
    }
    public String getLastName() {return lastName;}
    public void setLastName(String lastName) {this.lastName = lastName;}

    public Typing getTyping() {return typing;}
    public void setTyping(Typing typing) {this.typing = typing;}

    public int getId() { return id; }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDate() {return date;}

    public void setDate(Date date) {this.date = date;}

    public String getDescription() {return  description;}

    public void setDescription(String description) {this.description = description;}

    public double getAmount() {return amount;}

    public void setAmount(double amount) {this.amount = amount;}

    public int getTypingId() {return typingId;}

    public void setTypingId(int typingId) {this.typingId = typingId;}

    public int getEmployeeId() {return employeeId;}

    public void setEmployeeId(int employeeId) {this.employeeId = employeeId;}


    public void setfirstName(String firstName) {this.firstName = firstName;}
    public String getfirstName() {return firstName;}





}
