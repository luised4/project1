package com.models;

public enum Typing {
    PENDING,
    DENIED,
    ACCEPTED
}
