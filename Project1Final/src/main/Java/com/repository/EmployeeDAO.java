package com.repository;


import com.models.User;

public interface EmployeeDAO {
	


	User getUserByCredentials(String username, String password);


}
