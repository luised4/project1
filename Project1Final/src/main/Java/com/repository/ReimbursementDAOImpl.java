package com.repository;




import com.models.Employee;
import com.models.Reimbursement;
import com.models.Typing;
import com.system.BlabApplicationDataException;
import com.system.ConnectionManager;

import java.sql.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


public class ReimbursementDAOImpl implements ReimbDAO{

    private final ConnectionManager manager;

    public ReimbursementDAOImpl(ConnectionManager manager) {
        this.manager = manager;
    }

    public List<Reimbursement> FindReimburmentID(int employeeid) {

        List<Reimbursement> posts = new ArrayList<>();
        try(Connection c = manager.getConnection()) {
//            String sql = "SELECT * FROM blab_user WHERE email = " + email;
//            Statement s = c.createStatement();
//            ResultSet resultSet = s.executeQuery(sql);
            String sql = "select reimbursement_id,amount,description from  Reimbursement where reimbursement_id = ?" ;


            PreparedStatement s = c.prepareStatement(sql);
            s.setInt(1,employeeid);
            ResultSet rs = s.executeQuery();


            while(rs.next()) {
                Reimbursement u = new Reimbursement();
                u.setId(rs.getInt("reimbursement_id"));
                posts.add(u);

                Reimbursement p = new Reimbursement();
                p.setAmount(rs.getInt("amount"));

                posts.add(p);

                p.setDescription(rs.getString("description "));



            }
            return posts;

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return posts;
    }



    public int update(Reimbursement obj) {
        return 0;
    }

    public int create(Reimbursement obj) {


       // Random rand = new Random();

        //int n = rand.nextInt(50000);

        int id = 0;
        try(Connection c = manager.getConnection()) {
            String sql = "INSERT INTO  Reimbursement (employee_id,reimbursement_id,reimbursement_date,description, amount) " +
                    "VALUES (?,?,?) " +
                    "RETURNING (reimbursement_id)";
            //transaction_number
            PreparedStatement p1 = c.prepareStatement(sql);
            p1.setInt(1,obj.getId());
            p1.setInt(2, obj.getId());
            p1.setDouble(3, obj.getAmount());


            ResultSet rs = p1.executeQuery();


            while(rs.next()) {
                id = rs.getInt("reimbursement_id");
                obj.setId(id);
            }
            return id;

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return id;

    }


    public void delete(Reimbursement obj) {

    }


    public Reimbursement findById(int id) {
        return null;
    }


    public List<Reimbursement> findAll() {
        return null;
    }


    public Employee getByEmail(String email) {
        return null;
    }

    public int addReimbursement(Reimbursement obj) {
        int id = 0;
        LocalDate localDate = LocalDate.now();
        try(Connection c = manager.getConnection()) {
            String sql = "INSERT INTO reimbursement (employee_id,amount,description)" +
                    "VALUES (?,?,?)";
            //transaction_number
            PreparedStatement p1 = c.prepareStatement(sql);
            p1.setInt(1,obj.getId());
            p1.setDouble(2, obj.getAmount());
            p1.setString(3,obj.getDescription());

            //p1.setString(4,"Pending");
            p1.execute();


//            while(rs.next()) {
//                id = rs.getInt("reimbursement_id");
//                obj.setId(id);
//            }
//            return id;

        } catch (SQLException e){
            throw new BlabApplicationDataException("Failed to add Reimbursement", e );
        }
        return id;

    }
}



