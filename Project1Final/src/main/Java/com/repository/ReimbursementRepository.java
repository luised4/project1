package com.repository;

import com.models.Reimbursement;

import java.util.List;

public interface ReimbursementRepository {
    List<Reimbursement> findAllReimbursements(int id);
}
