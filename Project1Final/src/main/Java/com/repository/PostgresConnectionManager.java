package com.repository;

import com.system.BlabApplicationDataException;
import com.system.ConnectionManager;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import java.sql.Connection;
import java.sql.SQLException;

public class PostgresConnectionManager implements ConnectionManager {

    HikariDataSource ds;

    @Override
    public void init() {

        String configFile=PostgresConnectionManager.class.getClassLoader().getResource("db.properties").getPath();

        //cpm
        try {
            Class.forName("org.postgresql.Driver");

            HikariConfig config = new HikariConfig(configFile);
            ds = new HikariDataSource(config);

        } catch (ClassNotFoundException e) {
            throw new BlabApplicationDataException("PostgreSql drive not found on the classpath", e);
        }

    }

    @Override
    public Connection getConnection() {
        if(ds != null) {
            try {
                return ds.getConnection();
            } catch (SQLException e) {
                throw new BlabApplicationDataException("A connection couldn't be made to the database", e);
            }
        } else {
            try {
                return ds.getConnection();
            } catch (SQLException e) {
                throw new BlabApplicationDataException("A connection couldn't be made to the database", e);
            } catch (NullPointerException e) {
                throw new BlabApplicationDataException("An unexpected error has occurred", e);
            }
        }
    }
}
