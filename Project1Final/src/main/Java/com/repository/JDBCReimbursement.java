package com.repository;


import com.models.Reimbursement;
import com.models.Typing;
import com.system.BlabApplicationDataException;
import com.system.ConnectionManager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class JDBCReimbursement implements Repository<Reimbursement> {

    private final ConnectionManager manager;

    public JDBCReimbursement(ConnectionManager manager) {
        this.manager = manager;
    }

    public List<Reimbursement> FindReimburmentID(int employeeid) {

        List<Reimbursement> posts = new ArrayList<>();
        try(Connection c = manager.getConnection()) {
//            String sql = "SELECT * FROM blab_user WHERE email = " + email;
//            Statement s = c.createStatement();
//            ResultSet resultSet = s.executeQuery(sql);
            String sql = "select reimbursement_id,ammount from  Reimbursement where reimbursement_id = ?" ;


            PreparedStatement s = c.prepareStatement(sql);
            s.setInt(1,employeeid);
            ResultSet rs = s.executeQuery();


            while(rs.next()) {
                Reimbursement u = new Reimbursement();
                u.setId(rs.getInt("reimbursement_id"));
                Reimbursement p = new Reimbursement();
                p.setAmount(rs.getInt("amount"));


                posts.add(p);
            }
            return posts;

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return posts;
    }




    public int create(Reimbursement obj) {


       // Random rand = new Random();

        //int n = rand.nextInt(50000);

        int id = 0;
        try(Connection c = manager.getConnection()) {
            String sql = "INSERT INTO  Reimbursement (employee_id,reimbursement_id,reimbursement_date,description, ammount) " +
                    "VALUES (?,?,?) " +
                    "RETURNING (reimbursement_id)";
            //transaction_number
            PreparedStatement p1 = c.prepareStatement(sql);
            p1.setInt(1,obj.getId());
            p1.setInt(2, obj.getId());
            p1.setDouble(3, obj.getAmount());

            ResultSet rs = p1.executeQuery();


            while(rs.next()) {
                id = rs.getInt("reimbursement_id");
                obj.setId(id);
            }
            c.close();
            return id;

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return id;

    }

    @Override
    public int update(Reimbursement obj) {
        return 0;
    }

    @Override
    public void delete(Reimbursement obj) {

    }

    @Override
    public Reimbursement findById(int id) {
        return null;
    }

    @Override
    public List<Reimbursement> findAll() {
        return null;
    }


    @Override
    public List<Reimbursement> findAllReimbursements(int id) {
        List<Reimbursement> reimbursements = new ArrayList<>();

        try
            (Connection c = manager.getConnection()) {

            String sql = "SELECT reimbursement_id, reimbursement_date, description, amount, employee_id" +
                    " FROM reimbursement" +
                    " WHERE employee_id = ?";

            PreparedStatement ps = c.prepareStatement(sql);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();

            Reimbursement r = null;
            while (rs.next()) {
                r = new Reimbursement();
                r.setId(rs.getInt("reimbursement_id"));
                r.setDate(rs.getDate("reimbursement_date"));
                r.setDescription(rs.getString("description"));
                r.setAmount(rs.getDouble("amount"));
                r.setEmployeeId(rs.getInt("employee_id"));
                reimbursements.add(r);
            }
            c.close();
            return reimbursements;

        } catch (SQLException e) {
            throw new BlabApplicationDataException("Could not connect to Reimbursement Repository" + id, e);
        }
    }

    @Override
    public List<Reimbursement> findAllResolved() {
        List<Reimbursement> reimbursements = new ArrayList<>();

        try
                (Connection c = manager.getConnection()) {

            String sql = "SELECT reimbursement.reimbursement_date, employee.first_name, employee.last_name, reimbursement.amount, reimbursement.typing, employee.employee_id, reimbursement.description" +
                    " FROM reimbursement JOIN employee ON reimbursement.employee_id = employee.employee_id" +
                    " WHERE reimbursement.typing = 'Accepted' OR reimbursement.typing = 'Denied'";

            PreparedStatement ps = c.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();

            Reimbursement r = null;
            while(rs.next()) {
                r = new Reimbursement();
//                r.setId(rs.getInt("reimbursement_id"));
                r.setDate(rs.getDate("reimbursement_date"));
                r.setDescription(rs.getString("description"));
                r.setAmount(rs.getDouble("amount"));
//                r.setTypingId(rs.getInt("type_id"));
                r.setEmployeeId(rs.getInt("employee_id"));
                r.setfirstName(rs.getString("first_name"));
                r.setTyping(Typing.valueOf(rs.getString("typing").toUpperCase()));
                r.setLastName(rs.getString("last_name"));
                reimbursements.add(r);
                }
            c.close();
            return reimbursements;
            } catch (SQLException e) {
            throw new BlabApplicationDataException("Could not complete findAllResolved", e);
        }
    }

    @Override
    public List<Reimbursement> findAllPending() {
            List<Reimbursement> reimbursements = new ArrayList<>();

            try
                    (Connection c = manager.getConnection()) {

                String sql = "SELECT reimbursement.reimbursement_date, employee.first_name, employee.last_name, reimbursement.amount, reimbursement.typing, employee.employee_id, reimbursement.description" +
                        " FROM reimbursement JOIN employee ON reimbursement.employee_id = employee.employee_id" +
                        " WHERE typing = 'Pending'";

                PreparedStatement ps = c.prepareStatement(sql);
                ResultSet rs = ps.executeQuery();

                Reimbursement r = null;
                while(rs.next()) {
                    r = new Reimbursement();
//                r.setId(rs.getInt("reimbursement_id"));
                    r.setDate(rs.getDate("reimbursement_date"));
                    r.setDescription(rs.getString("description"));
                    r.setAmount(rs.getDouble("amount"));
//                r.setTypingId(rs.getInt("type_id"));
                    r.setEmployeeId(rs.getInt("employee_id"));
                    r.setfirstName(rs.getString("first_name"));
                    r.setTyping(Typing.valueOf(rs.getString("typing").toUpperCase()));
                    r.setLastName(rs.getString("last_name"));
                    reimbursements.add(r);
                }
                c.close();
                return reimbursements;
            } catch (SQLException e) {
                throw new BlabApplicationDataException("Could not complete findAllResolved", e);
            }
        }
    }




