package com.system;

import com.Service.*;
import com.models.Employee;
import com.models.Reimbursement;
import com.repository.*;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class ContextListener implements ServletContextListener {
    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        ServletContext context = servletContextEvent.getServletContext();
        System.out.println("Web Context Started for " + context.getInitParameter("ApplicationName"));

        ConnectionManager manager = new PostgresConnectionManager();
        manager.init();

        Repository<Employee> er = new JDBCEmployee(manager);
        UserDetailService uds = new UserDetailImpl((EmployeeRepository) er);
        context.setAttribute("userDetail", uds);

        Repository<Reimbursement> rr = new JDBCReimbursement(manager);
        ReimbursementService rs = new ReimbursementService();
        context.setAttribute("reimbursementService", rs);
        rs.setReimbursementRepository(rr);

        UserAuthentication uas = new UserAuthenticationImpl(uds);
        context.setAttribute("userAuthentication", uas);

        EmployeeDAOImpl employeeDAO= new EmployeeDAOImpl();
        UserService userService=new UserService();
        context.setAttribute("employeeDAO", employeeDAO);

    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {

    }
}
