package com.system;

public class BlabApplicationDataException extends RuntimeException {
    public BlabApplicationDataException(){}
    public BlabApplicationDataException(String msg) {
        super(msg);
    }
    public BlabApplicationDataException(String msg, Throwable cause){
        super(msg, cause);
    }
}
