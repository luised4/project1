package com.system;

import java.sql.Connection;

public interface ConnectionManager {
    void init();
    Connection getConnection();
}
