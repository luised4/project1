package com.system;

import com.models.Employee;

public interface AuthenticationStatus {
    void authStatus(Employee u, boolean success);
}
