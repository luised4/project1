package com.ex.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;





import com.ex.Service.ReimbursmentService;
import com.ex.models.Reimbursment;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONObject;


public class ReimbursementServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	int cnt;
	String returnMessage = null;

	ReimbursmentService reimbService = new ReimbursmentService();

	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		doPost(req,resp);


	}

	protected void doGet(HttpServletRequest request,
						 HttpServletResponse response) throws ServletException, IOException {
		service(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String requestParam= request.getReader().readLine();
		JSONObject c=new JSONObject(requestParam);
		response.setContentType("application/json");

		int ID=c.getInt("id");
		int amount=c.getInt("cost");
		String desc=c.getString("desc");


//		int ID = Integer.parseInt(request.getParameter("ID"));
//		int amount = Integer.parseInt(request.getParameter("amount"));

		Reimbursment r= new Reimbursment();
		r.setId(ID);
		r.setAmmount(amount);
		r.setDescription(desc);
		reimbService.addReimbursement(r);



		//RequestDispatcher dispatcher = request.getRequestDispatcher("Project1Attempt/LoginServlet");
		//dispatcher.forward(request, response);

	}
}
//		ReimbursmentService reimbService = new ReimbursmentService();
//		ObjectMapper mapper = new ObjectMapper();
//
//		Reimbursment reimbursment = mapper.readValue(request.getInputStream(), Reimbursment.class);
//
//		int reimb = reimbService.addReimbursement(reimbursment);
//
//		PrintWriter pw = response.getWriter();
//		response.setContentType("application/json");
//
//	}




