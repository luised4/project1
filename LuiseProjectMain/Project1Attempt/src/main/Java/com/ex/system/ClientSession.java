package com.ex.system;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class ClientSession implements Session {
    private Map<String, Object> sessionData;

    public ClientSession() {
        sessionData = new ConcurrentHashMap<>();
    }
    @Override
    public void setValue(String key, Object data) {
        sessionData.put(key, data);
    }

    @Override
    public Object getValue(String key) {
        return sessionData.get(key);
    }

    @Override
    public void removeValue(String key) {
        sessionData.remove(key);
    }

    @Override
    public void invalidate() {
        sessionData.clear();
    }
}
