package com.ex.system;

import com.ex.Service.*;
import com.ex.models.Employee;
import com.ex.models.User;
import com.ex.repository.*;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class ContextListener implements ServletContextListener {
    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        ServletContext context = servletContextEvent.getServletContext();
        System.out.println("Web Context Started for " + context.getInitParameter("ApplicationName"));

        ConnectionManager manager = new PostgresConnectionManager();
        manager.init();

        Repository<Employee> er = new JDBCEmployee(manager);
        UserDetailService uds = new UserDetailImpl((EmployeeRepository) er);
        context.setAttribute("userDetail", uds);

        UserAuthentication uas = new UserAuthenticationImpl(uds);
        context.setAttribute("userAuthentication", uas);

        EmployeeDAOImpl employeeDAO= new EmployeeDAOImpl();
        UserService userService=new UserService();
        context.setAttribute("employeeDAO", employeeDAO);




//
//       // EmployeeDAO p = new EmployeeDAOImpl(manager);
//        UserService ps = new UserService();
//        context.setAttribute("UserService", ps);




    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {

    }
}
