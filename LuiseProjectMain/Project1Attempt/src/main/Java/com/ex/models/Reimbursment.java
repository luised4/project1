package com.ex.models;

import java.time.LocalDate;
import java.util.Date;
import java.util.Objects;

public class Reimbursment {

    private int Id;
    private int deposit;
    private int phone_number;
    private String typeAccount;
    private LocalDate createDate;
    private int account_number;
    private String type;

    private int reimbursement_id;
    private String reimbursement_date;
    private String description ;
    private float amount ;

    public Reimbursment() {
    }

    public Reimbursment(int reimbursement_id, String reimbursement_date,String description ,float amount,String type ) {
        this.reimbursement_id= reimbursement_id;
        this.reimbursement_date = reimbursement_date;
        this. description =  description;
        this.amount=amount;
        this.type=type;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        this.Id = Id;
    }

    public int  getPhone() {
        return phone_number;    }

        public void setPhone(int phone_number){
        this.phone_number=phone_number;
    }
  //  public LocalDate getCreateDate() {

        public static java.sql.Date getCreateDate() {
            Date today = new Date();
            return new java.sql.Date(today.getTime());

    }
    public void setCreateDate(LocalDate createDate) {
        this.createDate = createDate;
    }


    public int getAccountN(){
        return account_number;
    }

    public void setAccount_number(int account_number) {
        this.account_number = account_number;
    }

    public String getTypeAccount() {
        return typeAccount;
    }

    public void setTypeAccount(String typeAccount) {
        this.typeAccount = typeAccount;
    }

    public int getAmmount(){
        return deposit;
    }
    public void setAmmount(int deposit) {
        this.deposit=deposit;
    }

    public String getDescription(){
        return description;
    }
    public void setDescription(String description) {
        this.description=description;
    }



    public String getDate(){
        return reimbursement_date;
    }
    public void setDate(String reimbursement_date) {
        this.reimbursement_date=reimbursement_date;
    }

    public String getType(){
        return type;
    }
    public void setType(String type) {
        this.type=type;
    }



    public String toString() {
        return
                "ammount =" + deposit + '\'' ;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Reimbursment deposit = (Reimbursment) o;
        return Id == deposit.Id &&

                Objects.equals(typeAccount,deposit.typeAccount)&&
                Objects.equals(deposit, deposit.deposit);

    }

    @Override
    public int hashCode() {
        return Objects.hash(Id,deposit,typeAccount);
    }



}
