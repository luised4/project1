package com.ex.repository;

import com.ex.models.User;

public interface EmployeeDAO {
	


	User getUserByCredentials(String username, String password);


}
