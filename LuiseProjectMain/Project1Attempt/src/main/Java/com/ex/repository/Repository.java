package com.ex.repository;


import com.ex.models.Employee;

import java.util.List;

public interface Repository<T> {

    int create(T obj);

    int update(T obj);
    void delete(T obj);
    T findById(int id);
    List<T> findAll();
    //Employee getCredentials(String username,password);
    Employee getByEmail(String email);


}
