package com.ex.repository;


import com.ex.models.Employee;
import com.ex.models.Reimbursment;
import com.ex.system.BlabApplicationDataException;
import com.ex.system.ConnectionManager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class ReimbursmentDAOImpl implements ReimbDAO{

    private final ConnectionManager manager;

    public ReimbursmentDAOImpl(ConnectionManager manager) {
        this.manager = manager;
    }

    public List<Reimbursment> FindReimburmentID(int employeeid) {

        List<Reimbursment> posts = new ArrayList<>();
        try(Connection c = manager.getConnection()) {
//            String sql = "SELECT * FROM blab_user WHERE email = " + email;
//            Statement s = c.createStatement();
//            ResultSet resultSet = s.executeQuery(sql);
            String sql = "select reimbursement_id,ammount,description from  Reimbursement where reimbursement_id = ?" ;


            PreparedStatement s = c.prepareStatement(sql);
            s.setInt(1,employeeid);
            ResultSet rs = s.executeQuery();


            while(rs.next()) {
                Reimbursment u = new Reimbursment();
                u.setId(rs.getInt("reimbursement_id"));
                posts.add(u);

                Reimbursment p = new Reimbursment();
                p.setAmmount(rs.getInt("amount"));

                posts.add(p);

                p.setDescription(rs.getString("description "));



            }
            return posts;

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return posts;
    }



    public int update(Reimbursment obj) {
        return 0;
    }

    public int create(Reimbursment obj) {


       // Random rand = new Random();

        //int n = rand.nextInt(50000);

        int id = 0;
        try(Connection c = manager.getConnection()) {
            String sql = "INSERT INTO  Reimbursement (employee_id,reimbursement_id,reimbursement_date,description, ammount) " +
                    "VALUES (?,?,?) " +
                    "RETURNING (reimbursement_id)";
            //transaction_number
            PreparedStatement p1 = c.prepareStatement(sql);
            p1.setInt(1,obj.getId());
            p1.setInt(2, obj.getId());
            p1.setInt(3, obj.getAmmount());


            ResultSet rs = p1.executeQuery();


            while(rs.next()) {
                id = rs.getInt("reimbursement_id");
                obj.setId(id);
            }
            return id;

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return id;

    }


    public void delete(Reimbursment obj) {

    }


    public Reimbursment findById(int id) {
        return null;
    }


    public List<Reimbursment> findAll() {
        return null;
    }


    public Employee getByEmail(String email) {
        return null;
    }

    public int addReimbursement(Reimbursment obj) {
        int id = 0;
        try(Connection c = manager.getConnection()) {
            String sql = "INSERT INTO reimbursement (employee_id,amount,description)" +
                    "VALUES (?,?,?)";
            //transaction_number
            PreparedStatement p1 = c.prepareStatement(sql);
            p1.setInt(1,obj.getId());
            p1.setInt(2, obj.getAmmount());
            p1.setString(3,obj.getDescription());
            //p1.setString(4,"Pending");
            p1.execute();


//            while(rs.next()) {
//                id = rs.getInt("reimbursement_id");
//                obj.setId(id);
//            }
//            return id;

        } catch (SQLException e){
            throw new BlabApplicationDataException("Failed to add Reimbursement", e );
        }
        return id;

    }
}



