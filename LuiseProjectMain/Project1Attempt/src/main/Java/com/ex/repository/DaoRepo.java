package com.ex.repository;


import com.ex.models.Employee;

import java.util.List;

public interface DaoRepo<T> {
    int create(T obj);
    void update(T obj);
    void delete(T obj);
    T findById(int id);

    List<Employee> findAll();
}
