package com.ex.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


import com.ex.models.User;
import com.ex.system.ConnectionManager;


public class EmployeeDAOImpl implements EmployeeDAO
{
	private ConnectionManager manager;
	private String username;

	public EmployeeDAOImpl(PostgresConnectionManager manager) {
		this.manager = manager;
	}

	public EmployeeDAOImpl() {

	}


	public User getUserByCredentials(String username, String password) {
		User u = new User();
		//System.out.println("getUserByCredentials() is green");
		try(Connection  conn = manager.getConnection())
		{
			String sql = "SELECT email, password, first_name, last_name" +
					" FROM employee";
//                    " WHERE employee_id = ?";

			PreparedStatement pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, username);
			pstmt.setString(2, password);

			ResultSet rs = pstmt.executeQuery();

			while(rs.next())
			{

				u.setUser_email(rs.getString("email"));
				u.setErs_password(rs.getString("password"));
				u.setUser_first_name(rs.getString("first_name"));
				u.setUser_last_name(rs.getString("last_name"));

			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(u.toString());
		return u;
	}
	/*

	@Override
	public User getUserByCredentials(String username, String password) {
		User u = new User();
		//System.out.println("getUserByCredentials() is green");
		try (Connection c = manager.getConnection()) {
			String sql = "SELECT email, password, first_name, last_name" +
					" FROM employee";
//                    " WHERE employee_id = ?";

			PreparedStatement ps = c.prepareStatement(sql);
//            ps.setInt(1, Id);
			ResultSet rs = ps.executeQuery();

			User e = null;
			while (rs.next()) {
				e = new User();
				e.setUser_email(rs.getString("email"));
				e.setErs_password();rs.getString("password"));
				e.getUser_first_name(rs.getString("first_name"));
				e.setUser_last_name(rs.getString("last_name"));
				break;
			}
			return e;
		} catch (SQLException e) {
			throw new BlabApplicationDataException("Could not connect to database", e);
		}
	}


*/



}
