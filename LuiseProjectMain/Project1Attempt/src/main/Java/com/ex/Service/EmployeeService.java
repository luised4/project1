package com.ex.Service;

import com.ex.models.Employee;
import com.ex.models.User;
import com.ex.repository.JDBCEmployee;

import java.util.List;

public class EmployeeService implements DataService<Employee> {
    private JDBCEmployee postRepository;


    public void setPostRepository(JDBCEmployee postRepository) {
        this.postRepository = postRepository;
    }

    @Override
    public Employee findOne(int id) {
        return null;
    }


    @Override
    public List<Employee> findAll() {
        return postRepository.findAll();
    }

    @Override
    public void update(Employee obj) {

    }


    @Override
    public void delete(Employee obj) {

    }

    @Override
    public void create(Employee obj) {

    }


//    public User loginUser(String username, String password) {
//        System.out.println("Made it to loginUser");
//        //System.out.println(u.toString());
//        User u = userDAO.getUserByCredentials(username, password);
//        //System.out.println(u.toString());
//        return u;
//    }


    @Override
    public int save(Employee obj) {
        if(obj == null)
            throw new IllegalArgumentException("obj can not be null");
        return postRepository.create(obj);
    }
}
