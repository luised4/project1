package com.ex.Service;


import com.ex.repository.PostgresConnectionManager;
import com.ex.repository.ReimbDAO;
import com.ex.repository.ReimbursmentDAOImpl;
import com.ex.repository.Repository;
import com.ex.models.Employee;
import com.ex.models.Reimbursment;
import com.ex.system.ConnectionManager;

import java.sql.Connection;
import java.util.List;

public class ReimbursmentService{

    private PostgresConnectionManager manager;
    private ReimbursmentDAOImpl dao;

    public ReimbursmentService(){
        manager = new PostgresConnectionManager();
        manager.init();
        dao = new ReimbursmentDAOImpl(manager);
    }

    public Reimbursment findOne(int id) {
        return null;
    }


    public List<Reimbursment> findAll() {
        return null;
    }


    public void update(Reimbursment obj) {

    }


    public void delete(Reimbursment obj) {

    }

    public int addReimbursement(Reimbursment obj) {
        return dao.addReimbursement(obj);

    }

    public int save(Reimbursment obj) {
        return 0;
    }
}