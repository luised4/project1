package com.ex.repository;
import com.ex.models.Employee;
import com.ex.system.BlabApplicationDataException;
import com.ex.system.ConnectionManager;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class JDBCEmployee implements Repository<Employee> {

    private final ConnectionManager manager;
    private int employee_id;
    String firstName;
    String lastName;
   // String email,
    String password;
    public JDBCEmployee(ConnectionManager manager) {
        this.manager = manager;
    }


    public int checkID(int Id) {

        try (Connection c = manager.getConnection()) {
            String sql = "select employee_id,email,first_name,last_name " +
                    "from employee where employee_id = ?";
            PreparedStatement p = c.prepareStatement(sql);
            p.setInt(1, Id);
            ResultSet rs = p.executeQuery();

            if (rs == null){
                return 0;
            }

            while (rs.next()){
                return rs.getInt("employee_id");
            }
            return 0;
        }catch(SQLException e){
            System.out.println("Checking employee_id Failed" + e);
        }
        return 0;
    }


    public int update(Employee obj) {
        return 0;
    }


    public void delete(Employee obj) {

    }

    @Override
    public Employee findById(int id) {
        return null;
    }

    @Override
    public List<Employee> findAll() {
        return null;
    }


    public int create(Employee obj) {

        int id = 0;
        try(Connection c = manager.getConnection()) {
            //  insert into userbank (ssn,email,first_name, last_name,day_of_birthday,account_type,password_user)
            String sql = "insert into userbank (ssn,email,first_name, last_name,day_of_birthday,password_user) " +
                    "VALUES (?,?,?,?,?,?) " +
                    "RETURNING ssn";
            PreparedStatement ps = c.prepareStatement(sql);
            ps.setInt(1, obj.getEmployee_id());
            ps.setString(2, obj.getEmail());
            ps.setString(3,obj.getFirstName());
            ps.setString(4,obj.getLastName());


            ps.setString(5, obj.getBirthday());

            ps.setString(6,obj.getPassword());



            ResultSet rs = ps.executeQuery();



            while(rs.next()) {
                id = rs.getInt("ssn");
                obj.setEmployee_id(id);
            }
            return id;

        } catch (SQLException e) {
            e.printStackTrace();
        }
       return id;

    }



    public Employee findByEmail(String email) {
        try(Connection c = manager.getConnection()) {

            String sql = "SELECT email,password from employee where email = ?" ;
            PreparedStatement ps = c.prepareStatement(sql);
            ps.setString(1, email);

            ResultSet rs = ps.executeQuery();
            //ssn,email,first_name, last_name,day_of_birthday,account_type,password_user
            Employee u = null;
            while(rs.next()) {

                u = new Employee(employee_id,email,password, firstName, lastName);
                u.setEmail(rs.getString("email"));
                u.setPassword(rs.getString("password"));
                //u.setActive(rs.getBoolean("is_active"));
                //u.setStatus(UserStatus.valueOf(rs.getString("status").toUpperCase()));
                break;
            }
            return u;

        } catch (SQLException e) {
            throw new BlabApplicationDataException("Could not connect to database", e);
        }
    }
}
