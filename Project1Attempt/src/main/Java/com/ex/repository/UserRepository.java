package com.ex.repository;

import com.ex.models.Employee;

public interface UserRepository extends Repository<Employee>{

    Employee findById(int id);

}
