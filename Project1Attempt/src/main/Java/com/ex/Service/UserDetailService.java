package com.ex.Service;
import com.ex.models.Employee;

public interface UserDetailService {
    //Employee getByEmail(String email);
    Employee getByEmail(String email);

    int checkSSN(int ssn);

     //Employee checkEmail(String email);
   // Employee getId(String id);
}
