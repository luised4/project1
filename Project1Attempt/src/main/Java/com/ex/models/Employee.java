package com.ex.models;



import java.util.Objects;

public class Employee {
    private int Id;
    private String firstName;
    private String lastName;
    private String email;
    private String password;
    private String typeAccount;
   // private String accountNumber;
    //private String Id= "";
    private int employee_id=0;
    private String birthday;
    private String accountNumber;
   private String username;

    public Employee() {

    }


    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Employee(int employee_id, String email, String password, String  firstName, String lastName) {
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        this.Id = Id;
    }


    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTypeAccount() {
        return typeAccount;
    }

    public void setTypeAccount(String typeAccount) {
        this.typeAccount = typeAccount;
    }



    public int getEmployee_id() {
        return employee_id;
    }

    public void setEmployee_id(int Social) {
        this.employee_id = employee_id;
    }



    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }


    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }


    @Override
    public String toString() {
        return "Employee{" +
                ", employee_id='" + employee_id + '\'' +
                ", email='" + email + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +

                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Employee employee = (Employee) o;
        return Id == employee.Id &&

                 Objects.equals(employee_id, employee.employee_id) &&
                Objects.equals(email, employee.email) &&

                Objects.equals(firstName, employee.firstName) &&
                Objects.equals(lastName, employee.lastName) &&

                Objects.equals(password, employee.password);
    }

    @Override
    public int hashCode() {
        return Objects.hash(Id,email, firstName, lastName,employee_id);
    }
}


