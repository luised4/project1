package com.ex.Service;

import com.ex.repository.JDBCEmployee;
import com.ex.models.Employee;

import java.util.List;

public class EmployeeService implements DataService<Employee> {
    private JDBCEmployee postRepository;

    public void setPostRepository(JDBCEmployee postRepository) {
        this.postRepository = postRepository;
    }

    @Override
    public Employee findOne(int id) {
        return null;
    }


    @Override
    public List<Employee> findAll() {
        return postRepository.findAll();
    }

    @Override
    public void update(Employee obj) {

    }


    @Override
    public void delete(Employee obj) {

    }

    @Override
    public void create(Employee obj) {

    }

    @Override
    public int save(Employee obj) {
        if(obj == null)
            throw new IllegalArgumentException("obj can not be null");
        return postRepository.create(obj);
    }
}
