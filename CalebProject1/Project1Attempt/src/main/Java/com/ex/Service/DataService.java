package com.ex.Service;


import com.ex.models.Employee;

import java.util.List;

public interface DataService<T> {
    T findOne(int id);

    List<T> findAll();
    void update(T obj);
    void delete(T obj);
    void create(Employee obj);
    int save(T obj);
}
