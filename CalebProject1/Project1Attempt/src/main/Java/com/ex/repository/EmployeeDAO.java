package com.ex.repository;

import com.ex.models.User;

public interface UserDAO {
	


	User getUserByCredentials(String username, String password);


}
