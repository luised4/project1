package com.ex.repository;


import java.util.List;

public interface Repository<T> {

    int create(T obj);

    int update(T obj);
    void delete(T obj);
    T findById(int id);
    List<T> findAll();
}
