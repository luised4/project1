package com.ex.system;

import com.ex.Service.UserAuthentication;
import com.ex.Service.UserAuthenticationImpl;
import com.ex.Service.UserDetailImpl;
import com.ex.Service.UserDetailService;
import com.ex.models.Employee;
import com.ex.repository.EmployeeRepository;
import com.ex.repository.JDBCEmployee;
import com.ex.repository.PostgresConnectionManager;
import com.ex.repository.Repository;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class ContextListener implements ServletContextListener {
    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        ServletContext context = servletContextEvent.getServletContext();
        System.out.println("Web Context Started for " + context.getInitParameter("ApplicationName"));

        ConnectionManager manager = new PostgresConnectionManager();
        manager.init();

        Repository<Employee> er = new JDBCEmployee(manager);
        UserDetailService uds = new UserDetailImpl((EmployeeRepository) er);
        context.setAttribute("userDetail", uds);

        UserAuthentication uas = new UserAuthenticationImpl(uds);
        context.setAttribute("userAuthentication", uas);
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {

    }
}
