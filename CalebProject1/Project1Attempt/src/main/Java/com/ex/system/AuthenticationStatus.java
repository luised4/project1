package com.ex.system;

import com.ex.models.Employee;

public interface AuthenticationStatus {
    void authStatus(Employee u, boolean success);
}
