package com.ex.Service;

import com.ex.models.Employee;
import com.ex.repository.EmployeeRepository;

import java.util.List;

public class UserDetailImpl implements UserDetailService {

    private final EmployeeRepository employeeRepository;
    public UserDetailImpl (EmployeeRepository employeeRepository) {this.employeeRepository = employeeRepository;}

    @Override
    public Employee getByEmail(String email) {
        return employeeRepository.getByEmail(email);
    }

    @Override
    public Employee getById(int Id) {
        return null;
    }

    @Override
    public List<Employee> findAll() {
        return employeeRepository.findAll();
    }

}
