package com.ex.Service;


import com.ex.system.AuthenticationStatus;

public interface UserAuthentication {
    void authenticate(String username, String password,
                      AuthenticationStatus onStatusChange);
}
