package com.ex.Service;

import com.ex.models.User;
import com.ex.repository.EmployeeDAOImpl;
import com.ex.repository.PostgresConnectionManager;
import com.ex.system.ConnectionManager;


public class UserService {

	EmployeeDAOImpl userDAO;
	PostgresConnectionManager manager;

	public UserService(){
		manager = new PostgresConnectionManager();
		manager.init();
		userDAO = new EmployeeDAOImpl(manager);
	}



	public User loginUser(String username, String password) {
		System.out.println("Log in well");
		User u = new User();

		u = userDAO.getUserByCredentials(username, password);

		return u;
	}
}
