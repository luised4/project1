package com.ex.Service;
import com.ex.models.Employee;

import java.util.List;

public interface UserDetailService {

    Employee getByEmail(String email);
    Employee getById (int Id);

    List<Employee> findAll();
}
