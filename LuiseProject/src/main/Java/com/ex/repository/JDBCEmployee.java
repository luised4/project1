package com.ex.repository;
import com.ex.models.Employee;
import com.ex.system.BlabApplicationDataException;
import com.ex.system.ConnectionManager;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class JDBCEmployee implements EmployeeRepository {

    private final ConnectionManager manager;
    private String username;

    public JDBCEmployee(ConnectionManager manager) {
        this.manager = manager;
    }

    private int Id;
    String firstName;
    String lastName;
    String email;
    String password;


//    public int checkID(int Id) {
//
//        try (Connection c = manager.getConnection()) {
//            String sql = "select employee_id,email,first_name,last_name " +
//                    "from employee where employee_id = ?";
//            PreparedStatement p = c.prepareStatement(sql);
//            p.setInt(1, Id);
//            ResultSet rs = p.executeQuery();
//
//            if (rs == null) {
//                return 0;
//            }
//
//            while (rs.next()) {
//                return rs.getInt("employee_id");
//            }
//            return 0;
//        } catch (SQLException e) {
//            System.out.println("Checking employee_id Failed" + e);
//        }
//        return 0;
//    }

//
//    public int update(Employee obj) {
//        return 0;
//    }
//
//
//    public void delete(Employee obj) {
//
//    }

    @Override
    public int create(Employee obj) {
        return 0;
    }

    @Override
    public int update(Employee obj) {
        return 0;
    }

    @Override
    public void delete(Employee obj) {

    }

    @Override
    public Employee findById(int Id) {

        try (Connection c = manager.getConnection()) {
            String sql = "SELECT email, password, first_name, last_name" +
                    " FROM employee";
//                    " WHERE employee_id = ?";

            PreparedStatement ps = c.prepareStatement(sql);
//            ps.setInt(1, Id);
            ResultSet rs = ps.executeQuery();

            Employee e = null;
            while (rs.next()) {
                e = new Employee();
                e.setEmail(rs.getString("email"));
                e.setPassword(rs.getString("password"));
                e.setFirstName(rs.getString("first_name"));
                e.setLastName(rs.getString("last_name"));
                break;
            }
            return e;
        } catch (SQLException e) {
            throw new BlabApplicationDataException("Could not connect to database", e);
        }
    }

    @Override
    public List<Employee> findAll() {


        List<Employee> employees = new ArrayList<Employee>();

        try (Connection c = manager.getConnection()) {
            String sql = "SELECT email, password, first_name, last_name" +
                    " FROM employee";
//                    " WHERE employee_id = ?";

            PreparedStatement ps = c.prepareStatement(sql);
//            ps.setInt(1, Id);
            ResultSet rs = ps.executeQuery();

            Employee e = null;
            while (rs.next()) {
                e = new Employee();
                e.setEmail(rs.getString("email"));
                e.setPassword(rs.getString("password"));
                e.setFirstName(rs.getString("first_name"));
                e.setLastName(rs.getString("last_name"));
                employees.add(e);
                break;
            }
            return employees;
        } catch (SQLException e) {
            throw new BlabApplicationDataException("Could not connect to database", e);
        }

    }


    public Employee getCredentials(String username, String password) {



        try (Connection c = manager.getConnection()) {
        String sql = "SELECT email, password, first_name, last_name" +
                " FROM employee";
//                    " WHERE employee_id = ?";

        PreparedStatement ps = c.prepareStatement(sql);
//            ps.setInt(1, Id);
        ResultSet rs = ps.executeQuery();

        Employee e = null;
        while (rs.next()) {
            e = new Employee();
            e.setEmail(rs.getString("email"));
            e.setPassword(rs.getString("password"));
            e.setFirstName(rs.getString("first_name"));
            e.setLastName(rs.getString("last_name"));
            break;
        }
        return e;
    } catch (SQLException e) {
        throw new BlabApplicationDataException("Could not connect to database", e);
    }
}

    @Override
    public Employee getByEmail(String email) {
        try (Connection c = manager.getConnection()) {
            String sql = "SELECT email, password, first_name, last_name" +
                    " FROM employee";
//                    " WHERE employee_id = ?";

            PreparedStatement ps = c.prepareStatement(sql);
//            ps.setInt(1, Id);
            ResultSet rs = ps.executeQuery();

            Employee e = null;
            while (rs.next()) {
                e = new Employee();
                e.setEmail(rs.getString("email"));
                e.setPassword(rs.getString("password"));
                e.setFirstName(rs.getString("first_name"));
                e.setLastName(rs.getString("last_name"));
                break;
            }
            return e;
        } catch (SQLException e) {
            throw new BlabApplicationDataException("Could not connect to database", e);
        }
    }
}


//    public int create(Employee obj) {
//
//        int id = 0;
//        try(Connection c = manager.getConnection()) {
//            //  insert into userbank (ssn,email,first_name, last_name,day_of_birthday,account_type,password_user)
//            String sql = "insert into userbank (ssn,email,first_name, last_name,day_of_birthday,password_user) " +
//                    "VALUES (?,?,?,?,?,?) " +
//                    "RETURNING ssn";
//            PreparedStatement ps = c.prepareStatement(sql);
//            ps.setInt(1, obj.getId());
//            ps.setString(2, obj.getEmail());
//            ps.setString(3,obj.getFirstName());
//            ps.setString(4,obj.getLastName());
//            ps.setString(6,obj.getPassword());
//
//
//
//            ResultSet rs = ps.executeQuery();
//
//
//
//            while(rs.next()) {
//                id = rs.getInt("ssn");
//                obj.setId(id);
//            }
//            return id;
//
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//       return id;
//
//    }



//    public Employee findByEmail(String email) {
//        try(Connection c = manager.getConnection()) {
//
//            String sql = "SELECT email,password from employee where email = ?" ;
//            PreparedStatement ps = c.prepareStatement(sql);
//            ps.setString(1, email);
//
//            ResultSet rs = ps.executeQuery();
//            //ssn,email,first_name, last_name,day_of_birthday,account_type,password_user
//            Employee u = null;
//            while(rs.next()) {
//
//                u = new Employee(Id,email,password, firstName, lastName);
//                u.setEmail(rs.getString("email"));
//                u.setPassword(rs.getString("password"));
//                //u.setActive(rs.getBoolean("is_active"));
//                //u.setStatus(UserStatus.valueOf(rs.getString("status").toUpperCase()));
//                break;
//            }
//            return u;
//
//        } catch (SQLException e) {
//            throw new BlabApplicationDataException("Could not connect to database", e);
//        }
//    }
//}
