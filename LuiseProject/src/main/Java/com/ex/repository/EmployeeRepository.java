package com.ex.repository;

import com.ex.models.Employee;

import java.util.List;

public interface EmployeeRepository extends Repository<Employee>{

    Employee findById(int id);

    List<Employee> findAll();
}
