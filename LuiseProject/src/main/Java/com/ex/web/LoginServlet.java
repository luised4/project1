package com.ex.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.ex.Service.UserAuthentication;
import com.ex.models.User;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ex.Service.UserService;
import org.json.JSONObject;


public class LoginServlet extends HttpServlet {
	//UserAuthentication authService;
	UserService authService;
	private static final long serialVersionUID = 1L;



	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		doPost(req,resp);


	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{

//		String response = restTemplate.postForObject(url, message, String.class);
//		return response;
		
		UserService userService = new UserService();

		String userCredentials= request.getReader().readLine();
		JSONObject c=new JSONObject(userCredentials);


		//String[] userCredentials = mapper.readValue(request.getInputStream(), String[].class);
		String username = c.getString("email");
		String password = c.getString("pass");
		//String password = userCredentials[1];


		User authUser = userService.loginUser(username, password);
		

		if (authUser != null) {
			response.addHeader("USER","false");
		}else{
			response.addHeader("USER", "true");
		}

		response.setStatus(200);
	}
}
